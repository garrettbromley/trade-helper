This EA is to help with simplifying the opening, managing, and closing of trades. When opening and closing trades, the safety needs to be off [F].

![Helper Icon 200x200.png](https://bitbucket.org/repo/7EzrK8g/images/858154138-Helper%20Icon%20200x200.png)

![Entire Chart.png](https://bitbucket.org/repo/7EzrK8g/images/681519041-Entire%20Chart.png)

![Long Live.png](https://bitbucket.org/repo/7EzrK8g/images/1771285969-Long%20Live.png)