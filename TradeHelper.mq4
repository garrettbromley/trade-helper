//+------------------------------------------------------------------+
//|                                             	 TradeHelper.mq4 |
//|                                  Copyright 2017, Garrett Bromley |
//|                                http://www.fxwealthmanagement.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Garrett Bromley"
#property link      "http://www.fxwealthmanagement.com"
#property version   "1.70"
#property description "This EA is to help with simplifying the opening,"
#property description "managing and closing of trades. When opening and"
#property description "closing trades, the safety needs to be off [F]"
#property icon "helperIcon.ico";
#property strict

enum onoff
{
   on  = 1,    // Yes
   off = 0,    // No
};

//--- input parameters
input string      H1             = "==================================="; // RSI ALERT PROPERTIES
input onoff       enableAlerts   = on;                      // Enable Alerts for Extreme RSI?
input onoff       enableNotify   = on;                      // Enable Notifications for Extreme RSI?
input int         upperExtreme   = 80;                      // Upper Extreme for RSI
input int         lowerExtreme   = 20;                      // Lower Extreme for RSI
input int         resetTrigger   = 10;                      // How far from Extreme Values to Reset?
input int         rsiPeriod      = 14;                      // Period for RSI
input string      H2             = "==================================="; // TP/SL PROPERTIES
input string      tpValue        = "20";                    // Default Take Profit [pips]
input string      slValue        = "20";                    // Default Stop Loss [pips]
input string      trailValue     = "20";                    // Default Trailing Stop Loss [pips]
input string      H3             = "==================================="; // SIZING PROPERTIES
input double      startSize      = 0.1;                     // Default Starting Size [lots]
input double      incrementSize  = 0.15;                    // Default Increment Size [lots]
input string      H4             = "==================================="; // OTHER PROPERTIES
input int         MGN            = 8675309;                 // Magic Number
input onoff       spreadInPips   = off;                     // Display Spread in Pips?
input int         theSlippage    = 15;                      // Allowed Slippage
input onoff       highResolution = on;                      // High Resolution Screen?
input string      H5             = "==================================="; // HISTORY PROPERTIES
input onoff       showToday      = on;                      // Show Profit/Loss from Today
input onoff       showYesterday  = on;                      // Show Profit/Loss from Yesterday
input onoff       showWeek       = on;                      // Show Profit/Loss from Week
input onoff       showMonth      = on;                      // Show Profit/Loss from Month
input onoff       showYear       = off;                      // Show Profit/Loss from Year
input onoff       showAllTime    = off;                      // Show Profit/Loss from All Time

//--- variables
int               ticket         = 0;                       // Ticket ID
string            oComment       = "by Trade Helper Pro";   // Order Comment
string            commentHeader  = "Copyright 2017 | Garrett Bromley | Trade Helper Pro";
string            newLine        = "\n";                    // New Line
string            curTP          = "";                      // Used to get current inputs of TP
string            curSL          = "";                      // Used to get current inputs of SL
string            curTrail       = "";                      // Used to get current inputs of Trailing Stop
string            curSize        = "";                      // Used to get current inputs of SL
string            commentString  = "";                      // The comment string
double            takeProfit     = 0;                       // Take Profit Level
double            stopLoss       = 0;                       // Stop Loss Level
double            lotSize        = 0;                       // Lot Size
double            rsi            = 0;                       // RSI
int               spread         = 0;                       // Spread (in points)
double            pipSpread      = 0;                       // Spread (in pips)
int               totalOrders    = 0;                       // Total Number of Orders
int               ordersOnPair   = 0;                       // Total Orders on this pair
double            totalLots      = 0;                       // Total Lots on current orders
double            pipDistance    = 0;                       // Total Pips on current orders
double            totalProfit    = 0;                       // Total Profit on current orders
double            totalReturn    = 0;                       // Total Return on current orders
double            tpLevel        = 0;                       // Used when getting highest price
double            slLevel        = 0;                       // Used when getting lowest price
double            longStop       = 0;                       // Used when setting single stop
double            shortStop      = 0;                       // Used when setting single stop
double            theTarget      = 0;                       // Used when setting single target
double            trailingStop   = 0;                       // Trailing stop in points
double            profitPerPip   = 0;                       // Calculation of profit/pip
double            potentialProfit= 0;                       // Potential profit
double            potentialLoss  = 0;                       // Potential loss
double            potReturn      = 0;                       // Potential Return
double            potReturnPerc  = 0;                       // Potential Return in percent
double            potCurReturn   = 0;                       // Potential Return current return
double            potCurLots     = 0;                       // Potential Return current lots
double            potDollarPerPip= 0;                       // Potential Return dollar per pip
double            potCurPips     = 0;                       // Potential Return Pips to TP
bool              alertTrigger   = true;                    // Alerts Allowed?
double            marginRequired = 0;                       // Margin Required to open 1 lot trade
bool              safetyMode     = true;                    // true = ON, false = OFF
bool              tpLinesDrawn   = false;                   // If the lines for TP are drawn
double            TPBuyLevel     = 0;                       // TP Level when drawing lines
double            TPSellLevel    = 0;                       // TP Level when drawing lines
double            breakEvenPrice = 0;                       // Break Even Price for Display
bool              beLineDrawn    = false;                   // If the line for BE level is drawn
string            currentDir     = "NONE";                  // Contains the current direction of trading                 
bool              buyButtonThere = true;                    // If the buy button exists
bool              sellButtonThere= true;                    // If the sell button exists
string            currentMode    = "TOP";                   // Current TP/SL Mode (BUY > TOP, SELL > BOTTOM)
double            topEntry       = 0;                       // Top Entry Level
double            bottomEntry    = 0;                       // Bottom Entry Level
double            plToday        = 0;                       // Profit/Loss for Today
double            plYesterday    = 0;                       // Profit/Loss for Yesterday
double            plWeek         = 0;                       // Profit/Loss for Week
double            plMonth        = 0;                       // Profit/Loss for Month
double            plYTD          = 0;                       // Profit/Loss for YTD
double            plAllTime      = 0;                       // Profit/Loss for All Time

//--- Button Parameters
ENUM_BASE_CORNER  butCorner      = CORNER_RIGHT_UPPER;      // Chart corner for anchoring
string            butFont        = "Arial";                 // Font
int               butFontSize    = 12;                      // Font size
color             butColor       = clrBlack;                // Text color
color             butBackColor   = C'236,233,216';          // Background color
color             butBorderColor = clrNONE;                 // Border color
bool              butState       = false;                   // Pressed/Released
bool              butBack        = false;                   // Background object
bool              butSelection   = false;                   // Highlight to move
bool              butHidden      = true;                    // Hidden in the object list
long              butZOrder      = 0;                       // Priority for mouse click

string            buyButton      = "BUY";                   // Button name
string            sellButton     = "SELL";                  // Button name
string            modeButton     = "MODE";                  // Button name
string            closeButton    = "CLOSEALL";              // Button name
string            profitButton   = "CLOSEPROFIT";           // Button name
string            addTPButton    = "ADDTP";                 // Button Name
string            remTPButton    = "REMTP";                 // Button Name
string            addSLButton    = "ADDSL";                 // Button Name
string            remSLButton    = "REMSL";                 // Button Name
string            addTrailButton = "ADDTRAIL";              // Button Name
string            remTrailButton = "REMTRAIL";              // Button name
string            safetyButton   = "SAFETY";                // Button name

//--- Label Parameters
string            labelFont      = "Arial";                 // Font
int               labelFontSize  = 10;                      // Font size
color             labelColor     = clrRed;                  // Color
double            labelAngle     = 0.0;                     // Slope angle in degrees
ENUM_ANCHOR_POINT labelAnchor    = ANCHOR_RIGHT_UPPER;      // Anchor type
bool              labelBack      = false;                   // Background object
bool              labelSelection = false;                    // Highlight to move
bool              labelHidden    = true;                    // Hidden in the object list
long              labelZOrder    = 0;                       // Priority for mouse click

string            topPipsName    = "TOPPIPSNAME";           // Label name
string            bottomPipsName = "BOTTOMPIPSNAME";        // Label name
string            lotsName       = "LOTSNAME";              // Label name
string            spreadName     = "SPREADNAME";            // Label name

string            topPipsValue   = "TOPPIPSVALUE";          // Label name
string            bottomPipsValue= "BOTTOMPIPSVALUE";       // Label name
string            lotsValue      = "LOTSVALUE";             // Label name
string            spreadValue    = "SPREADVALUE";           // Label name

string            takeProfitName = "TAKEPROFITNAME";        // Label name
string            potentialProfitName="POTENTIALPROFIT";    // Label name
string            stopLossName   = "STOPLOSSNAME";          // Label name
string            potentialLossName="POTENTIALLOSS";        // Label name
string            trailStopName  = "TRAILSTOPNAME";         // Label name
string            lotSizeName    = "LOTSIZENAME";           // Label name
string            marginReqName  = "MARGINREQNAME";         // Label name

string            returnValue    = "RETURNVALUE";           // Label name
string            potentialReturn= "POTRETURNVALUE";        // Label name
string            potentialSLReturn="POTENTIALSLRETURN";    // Label name

//--- Edit Parameters
string            editFont        = "Arial";                // Font
int               editFontSize    = 10;                     // Font size
ENUM_ALIGN_MODE   editAlign       = ALIGN_LEFT;             // Text alignment type
bool              editReadOnly    = false;                  // Ability to edit
ENUM_BASE_CORNER  editCorner      = CORNER_RIGHT_UPPER;     // Chart corner for anchoring
color             editColor       = clrBlack;               // Text color
color             editBackColor   = clrWhite;               // Background color
color             editBorderColor = clrBlack;               // Border color
bool              editBack        = false;                  // Background object
bool              editSelection   = false;                  // Highlight to move
bool              editHidden      = true;                   // Hidden in the object list
long              editZOrder      = 0;                      // Priority for mouse click

string            tpName         = "TP";                    // Object name
string            slName         = "SL";                    // Object name
string            trailName      = "TRAIL";                 // Object name
string            sizeName       = "SIZE";                  // Object name

//--- Horizontal Line Parameters
int               HLinePrice     = 1.0;                     // Line price
color             HLineColor     = clrLimeGreen;            // Line color
ENUM_LINE_STYLE   HLineStyle     = STYLE_DASH;              // Line style
int               HLineWidth     = 1;                       // Line width
bool              HLineBack      = false;                   // Background line
bool              HLineSelection = false;                   // Highlight to move
bool              HLineHidden    = true;                    // Hidden in the object list
long              HLineZOrder    = 0;                       // Priority for mouse click

string            TPLineBUY      = "BUYTPLINE";             // Line name
string            TPLineSELL     = "SELLTPLINE";            // Line Name
string            BELine         = "BREAKEVENLINE";         // Line Name

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {

   returnHistory();
      
   if (highResolution) {
      commentString = newLine + commentHeader + newLine;
      if (showToday)    { commentString = commentString + newLine + "P/L Today: $"     + DoubleToString(plToday, 2) + newLine;    }
      if (showYesterday){ commentString = commentString + newLine + "P/L Yesterday: $" + DoubleToString(plYesterday, 2) + newLine;}
      if (showWeek)     { commentString = commentString + newLine + "P/L Week: $"      + DoubleToString(plWeek, 2) + newLine;     }
      if (showMonth)    { commentString = commentString + newLine + "P/L Month: $"     + DoubleToString(plMonth, 2) + newLine;    }
      if (showYear)     { commentString = commentString + newLine + "P/L Year: $"      + DoubleToString(plYTD, 2) + newLine;      }
      if (showAllTime)  { commentString = commentString + newLine + "P/L All Time: $"  + DoubleToString(plAllTime, 2) + newLine;  }
      Comment(commentString);
   } else {
      commentString = commentHeader;
      if (showToday)    { commentString = commentString + newLine + "P/L Today: $"     + DoubleToString(plToday, 2);    }
      if (showYesterday){ commentString = commentString + newLine + "P/L Yesterday: $" + DoubleToString(plYesterday, 2);}
      if (showWeek)     { commentString = commentString + newLine + "P/L Week: $"      + DoubleToString(plWeek, 2);     }
      if (showMonth)    { commentString = commentString + newLine + "P/L Month: $"     + DoubleToString(plMonth, 2);    }
      if (showYear)     { commentString = commentString + newLine + "P/L Year: $"      + DoubleToString(plYTD, 2);      }
      if (showAllTime)  { commentString = commentString + newLine + "P/L All Time: $"  + DoubleToString(plAllTime, 2);  }
      Comment(commentString);
   }   
   
   // See if the chart is currently shifted
   bool isShifted = false;
   ChartShiftGet(isShifted, 0);
   // If it is not shifted, then force it
   if (!isShifted) { ChartShiftSet(10, 0); }
   
   // Get the largest trade size currently
   double largest = startSize;
   if (OrdersTotal() > 0) {
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in onInit"); }
         
         if (OrderSymbol() == Symbol() && (OrderType() == OP_BUY || OrderType() == OP_SELL)) {
            if (OrderLots() > largest) { largest = OrderLots(); }
         }
      }
      
      largest = largest + incrementSize;
   }   
   
   // Create all the buttons and stuff
   if (highResolution) {
      ButtonCreate(0, buyButton, 0, 210, 50, 90, 50, butCorner, "BUY", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, sellButton, 0, 100, 50, 90, 50, butCorner, "SELL", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, profitButton, 0, 210, 440, 200, 35, butCorner, "CLOSE PROFIT", butFont, 9, butColor, butBackColor, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, closeButton, 0, 210, 490, 200, 35, butCorner, "CLOSE ALL", butFont, butFontSize, butColor, butBackColor, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      
      LabelCreate(0, topPipsName, 0, 100, 110, butCorner, "[T] PIPS:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, bottomPipsName, 0, 100, 150, butCorner, "[B] PIPS:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, lotsName, 0, 100, 190, butCorner, "LOTS:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, spreadName, 0, 100, 230, butCorner, "SPREAD:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      
      LabelCreate(0, topPipsValue, 0, 10, 110, butCorner, "", labelFont, labelFontSize, clrLimeGreen, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, bottomPipsValue, 0, 10, 150, butCorner, "", labelFont, labelFontSize, clrLimeGreen, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, lotsValue, 0, 10, 190, butCorner, "", labelFont, labelFontSize, clrRed, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, spreadValue, 0, 10, 230, butCorner, "", labelFont, labelFontSize, clrAqua, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      
      ButtonCreate(0, addTPButton, 0, 200, 275, 20, 20, butCorner, "+", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, remTPButton, 0, 170, 275, 20, 20, butCorner, "-", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, addSLButton, 0, 200, 315, 20, 20, butCorner, "+", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, remSLButton, 0, 170, 315, 20, 20, butCorner, "-", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, addTrailButton, 0, 240, 355, 20, 20, butCorner, "+", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, remTrailButton, 0, 210, 355, 20, 20, butCorner, "-", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      
      ButtonCreate(0, safetyButton, 0, 250, 440, 30, 85, butCorner, "S", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      
      LabelCreate(0, takeProfitName, 0, 100, 270, butCorner, "TP:", labelFont, labelFontSize, clrLimeGreen, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, potentialProfitName, 0, 215, 275, butCorner, "", labelFont, 8, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, stopLossName, 0, 100, 310, butCorner, "SL:", labelFont, labelFontSize, clrRed, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, potentialLossName, 0, 215, 315, butCorner, "", labelFont, 8, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, trailStopName, 0, 100, 350, butCorner, "TRAIL:", labelFont, labelFontSize, clrRed, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, lotSizeName, 0, 100, 390, butCorner, "SIZE:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, marginReqName, 0, 175, 395, butCorner, "", labelFont, 8, clrAqua, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      
      EditCreate(0, tpName, 0, 80, 270, 70, 35, tpValue, editFont, editFontSize, editAlign, editReadOnly, editCorner, editColor, editBackColor, editBorderColor, editBack, editSelection, editHidden, editZOrder);
      EditCreate(0, slName, 0, 80, 310, 70, 35, slValue, editFont, editFontSize, editAlign, editReadOnly, editCorner, editColor, editBackColor, editBorderColor, editBack, editSelection, editHidden, editZOrder);
      EditCreate(0, trailName, 0, 80, 350, 70, 35, trailValue, editFont, editFontSize, editAlign, editReadOnly, editCorner, editColor, editBackColor, editBorderColor, editBack, editSelection, editHidden, editZOrder);
      EditCreate(0, sizeName, 0, 80, 390, 70, 35, DoubleToString(largest, 2), editFont, editFontSize, editAlign, editReadOnly, editCorner, editColor, editBackColor, editBorderColor, editBack, editSelection, editHidden, editZOrder);
      
      LabelCreate(0, returnValue, 0, 10, 540, butCorner, "0.00%", labelFont, labelFontSize, clrLimeGreen, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, potentialReturn, 0, 10, 580, butCorner, "TP: $100.00 (50.00%)", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, potentialSLReturn, 0, 10, 620, butCorner, "SL: $100.00 (50.00%)", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
   } else {
      ButtonCreate(0, buyButton, 0, 115, 20, 50, 20, butCorner, "BUY", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, sellButton, 0, 55, 20, 50, 20, butCorner, "SELL", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, profitButton, 0, 115, 255, 110, 30, butCorner, "CLOSE PROFIT", butFont, 9, butColor, butBackColor, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, closeButton, 0, 115, 285, 110, 30, butCorner, "CLOSE ALL", butFont, butFontSize, butColor, butBackColor, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      
      LabelCreate(0, topPipsName, 0, 65, 45, butCorner, "[T] PIPS:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, bottomPipsName, 0, 65, 65, butCorner, "[B] PIPS:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, lotsName, 0, 65, 85, butCorner, "LOTS:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, spreadName, 0, 65, 105, butCorner, "SPREAD:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      
      LabelCreate(0, topPipsValue, 0, 10, 45, butCorner, "", labelFont, labelFontSize, clrLimeGreen, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, bottomPipsValue, 0, 10, 65, butCorner, "", labelFont, labelFontSize, clrLimeGreen, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, lotsValue, 0, 10, 85, butCorner, "", labelFont, labelFontSize, clrRed, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, spreadValue, 0, 10, 105, butCorner, "", labelFont, labelFontSize, clrAqua, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      
      ButtonCreate(0, addTPButton, 0, 130, 130, 15, 15, butCorner, "+", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, remTPButton, 0, 110, 130, 15, 15, butCorner, "-", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, addSLButton, 0, 130, 160, 15, 15, butCorner, "+", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, remSLButton, 0, 110, 160, 15, 15, butCorner, "-", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, addTrailButton, 0, 150, 190, 15, 15, butCorner, "+", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      ButtonCreate(0, remTrailButton, 0, 130, 190, 15, 15, butCorner, "-", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      
      ButtonCreate(0, safetyButton, 0, 145, 250, 25, 65, butCorner, "S", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
      
      LabelCreate(0, takeProfitName, 0, 65, 130, butCorner, "TP:", labelFont, labelFontSize, clrLimeGreen, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, potentialProfitName, 0, 140, 130, butCorner, "", labelFont, 8, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, stopLossName, 0, 65, 160, butCorner, "SL:", labelFont, labelFontSize, clrRed, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, potentialLossName, 0, 140, 160, butCorner, "", labelFont, 8, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, trailStopName, 0, 65, 190, butCorner, "TRAIL:", labelFont, labelFontSize, clrRed, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, lotSizeName, 0, 65, 220, butCorner, "SIZE:", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, marginReqName, 0, 110, 220, butCorner, "", labelFont, 8, clrAqua, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      
      EditCreate(0, tpName, 0, 45, 125, 40, 25, tpValue, editFont, editFontSize, editAlign, editReadOnly, editCorner, editColor, editBackColor, editBorderColor, editBack, editSelection, editHidden, editZOrder);
      EditCreate(0, slName, 0, 45, 155, 40, 25, slValue, editFont, editFontSize, editAlign, editReadOnly, editCorner, editColor, editBackColor, editBorderColor, editBack, editSelection, editHidden, editZOrder);
      EditCreate(0, trailName, 0, 45, 185, 40, 25, trailValue, editFont, editFontSize, editAlign, editReadOnly, editCorner, editColor, editBackColor, editBorderColor, editBack, editSelection, editHidden, editZOrder);
      EditCreate(0, sizeName, 0, 45, 215, 40, 25, DoubleToString(largest, 2), editFont, editFontSize, editAlign, editReadOnly, editCorner, editColor, editBackColor, editBorderColor, editBack, editSelection, editHidden, editZOrder);
      
      LabelCreate(0, returnValue, 0, 10, 320, butCorner, "0.00%", labelFont, labelFontSize, clrLimeGreen, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, potentialReturn, 0, 10, 335, butCorner, "TP: $100.00 (50.00%)", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
      LabelCreate(0, potentialSLReturn, 0, 10, 350, butCorner, "SL: $100.00 (50.00%)", labelFont, labelFontSize, clrWhite, labelAngle, labelAnchor, labelBack, labelSelection, labelHidden, labelZOrder);
   }
   
   ChartRedraw();
   
   return(INIT_SUCCEEDED);
}



//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
   // Delete all the buttons and stuff
   if (buyButtonThere) { ButtonDelete(0, buyButton); }
   if (sellButtonThere) { ButtonDelete(0, sellButton); }
   if (sellButtonThere == false || buyButtonThere == false) {
      ButtonDelete(0, modeButton);
   }
   ButtonDelete(0, closeButton);
   ButtonDelete(0, profitButton);
   
   LabelDelete(0, topPipsName);
   LabelDelete(0, bottomPipsName);
   LabelDelete(0, lotsName);
   LabelDelete(0, spreadName);
   
   LabelDelete(0, topPipsValue);
   LabelDelete(0, bottomPipsValue);
   LabelDelete(0, lotsValue);
   LabelDelete(0, spreadValue);
   
   ButtonDelete(0, addTPButton);
   ButtonDelete(0, remTPButton);
   ButtonDelete(0, addSLButton);
   ButtonDelete(0, remSLButton);
   ButtonDelete(0, addTrailButton);
   ButtonDelete(0, remTrailButton);
   
   ButtonDelete(0, safetyButton);
   
   LabelDelete(0, takeProfitName);
   LabelDelete(0, potentialProfitName);
   LabelDelete(0, stopLossName);
   LabelDelete(0, potentialLossName);
   LabelDelete(0, trailStopName);
   LabelDelete(0, lotSizeName);
   LabelDelete(0, marginReqName);
   
   EditDelete(0, tpName);
   EditDelete(0, slName);
   EditDelete(0, trailName);
   EditDelete(0, sizeName);
   
   LabelDelete(0, returnValue);
   LabelDelete(0, potentialReturn);
   LabelDelete(0, potentialSLReturn);
   
   if (beLineDrawn) { HLineDelete(0, BELine); }
   if (tpLinesDrawn) {
      HLineDelete(0, TPLineBUY);
      HLineDelete(0, TPLineSELL);
   }
}




//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick() {   
   // If lines don't exist anymore, turn the switches off
   if (beLineDrawn) {
      if (ObjectFind(0, BELine) < 0) { beLineDrawn = false; }
   }
   
   if (tpLinesDrawn) {
      if (ObjectFind(0, TPLineBUY) < 0 || ObjectFind(0, TPLineSELL)) { tpLinesDrawn = false; }
   }
   
   // Check if there are any trades on this pair, if not turn off trailing stop
   // Count how many orders we have
   if (totalOrders != OrdersTotal()) {
      // Total orders has changed, calculate differences
      returnHistory();
      
      if (highResolution) {
         commentString = newLine + commentHeader;
         if (showToday)    { commentString = commentString + newLine + "P/L Today: $"     + DoubleToString(plToday, 2);    }
         if (showYesterday){ commentString = commentString + newLine + "P/L Yesterday: $" + DoubleToString(plYesterday, 2);}
         if (showWeek)     { commentString = commentString + newLine + "P/L Week: $"      + DoubleToString(plWeek, 2);     }
         if (showMonth)    { commentString = commentString + newLine + "P/L Month: $"     + DoubleToString(plMonth, 2);    }
         if (showYear)     { commentString = commentString + newLine + "P/L Year: $"      + DoubleToString(plYTD, 2);      }
         if (showAllTime)  { commentString = commentString + newLine + "P/L All Time: $"  + DoubleToString(plAllTime, 2);  }
         Comment(commentString);
      } else {
         commentString = commentHeader;
         if (showToday)    { commentString = commentString + newLine + "P/L Today: $"     + DoubleToString(plToday, 2);    }
         if (showYesterday){ commentString = commentString + newLine + "P/L Yesterday: $" + DoubleToString(plYesterday, 2);}
         if (showWeek)     { commentString = commentString + newLine + "P/L Week: $"      + DoubleToString(plWeek, 2);     }
         if (showMonth)    { commentString = commentString + newLine + "P/L Month: $"     + DoubleToString(plMonth, 2);    }
         if (showYear)     { commentString = commentString + newLine + "P/L Year: $"      + DoubleToString(plYTD, 2);      }
         if (showAllTime)  { commentString = commentString + newLine + "P/L All Time: $"  + DoubleToString(plAllTime, 2);  }
         Comment(commentString);
      }   
      
      ordersOnPair = 0;
      
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in onTick"); }
         
         if (OrderSymbol() == Symbol() && (OrderType() == OP_BUY || OrderType() == OP_SELL)) {
            ordersOnPair++;
         }
      }
      
      totalOrders = OrdersTotal();
   }
   
   if (ordersOnPair == 0) { 
      trailingStop = 0; 
      currentDir = "NONE";
      
      if (buyButtonThere == false) { 
         ButtonDelete(0, modeButton);
         if (highResolution) {
            ButtonCreate(0, buyButton, 0, 210, 50, 90, 50, butCorner, "BUY", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder); 
         } else {
            ButtonCreate(0, buyButton, 0, 115, 20, 50, 20, butCorner, "BUY", butFont, butFontSize, butColor, clrLimeGreen, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder); 
         }
         buyButtonThere = true;
         EditTextChange(0, sizeName, DoubleToString(startSize, 2));
      }
      if (sellButtonThere == false) { 
         ButtonDelete(0, modeButton);
         if (highResolution) {
            ButtonCreate(0, sellButton, 0, 100, 50, 90, 50, butCorner, "SELL", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder); 
         } else {
            ButtonCreate(0, sellButton, 0, 55, 20, 50, 20, butCorner, "SELL", butFont, butFontSize, butColor, clrRed, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder); 
         }
         sellButtonThere = true; 
         EditTextChange(0, sizeName, DoubleToString(startSize, 2));
      }
   }   
   
   // Trail Stops if Needed
   if (trailingStop > 0) {
      TrailAllOrders(trailingStop * 10);
   }

   // Update RSI
   rsi = iRSI(Symbol(), Period(), rsiPeriod, PRICE_CLOSE, 0);
   
   if (rsi > lowerExtreme + resetTrigger && rsi < upperExtreme - resetTrigger) { alertTrigger = true; }
   
   if (alertTrigger && (rsi > upperExtreme || rsi < lowerExtreme)) {
      if (enableAlerts) { Alert("Helper: Check RSI [" + DoubleToString(rsi, 2) + "]"); }
      if (enableNotify) { SendNotification("Helper: Check RSI [" + DoubleToString(rsi, 2) + "]"); }
      alertTrigger = false;
   }
   
   // Update Spread
   spread = (int)MarketInfo(Symbol(),MODE_SPREAD);
   pipSpread = spread / 10;  // Convert to pips
   if (spreadInPips) {
      LabelTextChange(0, spreadValue, DoubleToString(pipSpread, 2));    // Pips
   } else {
      LabelTextChange(0, spreadValue, IntegerToString(spread));    // Points
   }
   
   // Calculate potential profit/loss
   EditTextGet(curTP, 0, tpName);
   EditTextGet(curSL, 0, slName);
   EditTextGet(curSize, 0, sizeName);
        
   takeProfit = StringToDouble(curTP);
   stopLoss = StringToDouble(curSL);
   lotSize = StringToDouble(curSize);
   
   if (lotSize > 0) {
      profitPerPip = ((((MarketInfo(Symbol(),MODE_TICKVALUE)* Point) / MarketInfo(Symbol(), MODE_TICKSIZE)) * lotSize) * 10);
      
      // Calculate margin required to open trade
      if (lotSize >= 1) {
         marginRequired = MarketInfo(Symbol(), MODE_MARGINREQUIRED) / lotSize;
      } else {
         marginRequired = MarketInfo(Symbol(), MODE_MARGINREQUIRED) * lotSize;
      }
      LabelTextChange(0, marginReqName, "[" + DoubleToString(marginRequired, 2) + "]");
   } else {
      LabelTextChange(0, marginReqName, "");
   }
   
   // Calculate potential profit
   if (profitPerPip > 0 && takeProfit > 0) {
      potentialProfit = profitPerPip * takeProfit;
      LabelTextChange(0, potentialProfitName, "$" + DoubleToString(potentialProfit, 2));
   } else {
      LabelTextChange(0, potentialProfitName, "");
   }
   
   // Calculate potential loss
   if (profitPerPip > 0 && stopLoss > 0) {
      potentialLoss = profitPerPip * stopLoss;
      LabelTextChange(0, potentialLossName, "-$" + DoubleToString(potentialLoss, 2));
   } else {
      LabelTextChange(0, potentialLossName, "");
   }
   
   // Show Break Even or Not (2 Trades Required)
   if (ordersOnPair > 1) {
      breakEvenPrice = CalculateBreakEven();
      
      if (beLineDrawn) {
         HLineMove(0, BELine, breakEvenPrice);
      } else {
         HLineCreate(0, BELine, 0, breakEvenPrice, clrAqua, HLineStyle, HLineWidth, HLineBack, HLineSelection, HLineHidden, HLineZOrder);
         beLineDrawn = true;
      }
   } else {
      if (beLineDrawn) {
         HLineDelete(0, BELine);
         beLineDrawn = false;
      }
   }
   
   // If we have open positions, calculate stuff
   if (ordersOnPair > 0) {
      // If TP Lines have been drawn, delete them
      if (tpLinesDrawn) {
         HLineDelete(0, TPLineBUY);
         HLineDelete(0, TPLineSELL);
         tpLinesDrawn = false;
      }
      
      // If BUY/SELL buttons both exist, figure out current direction and set things up
      if (buyButtonThere && sellButtonThere) {
         int openLongs = 0;
         int openShorts = 0;
         
         for(int i = OrdersTotal() - 1; i >= 0; i--) {
            if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in onTick"); }
            
            if (OrderSymbol() == Symbol()) {
               if (OrderType() == OP_BUY) { openLongs = openLongs + 1; }
               if (OrderType() == OP_SELL) { openShorts = openShorts + 1; }
            }
         }
         
         if (openLongs > openShorts) {
            currentDir = "LONG";
            // Delete the short button if needed
            ButtonDelete(0, sellButton); 
            sellButtonThere = false;
            
            // Create the mode switch
            currentMode = "TOP";
            if (highResolution) {
               ButtonCreate(0, modeButton, 0, 100, 50, 90, 50, butCorner, currentMode, butFont, 8, butColor, clrAqua, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
            } else {
               ButtonCreate(0, modeButton, 0, 55, 20, 50, 20, butCorner, currentMode, butFont, 8, butColor, clrAqua, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
            }
         } else {
            currentDir = "SHORT";
            // Delete the short button if needed
            ButtonDelete(0, buyButton); 
            buyButtonThere = false;
            
            // Create the mode switch
            currentMode = "BOTTOM";
            if (highResolution) {
               ButtonCreate(0, modeButton, 0, 210, 50, 90, 50, butCorner, currentMode, butFont, 8, butColor, clrAqua, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
            } else {
               ButtonCreate(0, modeButton, 0, 115, 20, 50, 20, butCorner, currentMode, butFont, 8, butColor, clrAqua, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
            }
         }
      }
         
      // Add together lots
      totalLots = 0;
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in onTick"); }
         
         if (OrderSymbol() == Symbol() && (OrderType() == OP_BUY || OrderType() == OP_SELL)) {
            totalLots = totalLots + OrderLots();
         }
      }
      
      LabelTextChange(0, lotsValue, DoubleToString(totalLots, 2));
      
      // Add together pips
      topEntry = GetTopTrade();
      bottomEntry = GetBottomTrade();
      
      // Use Bid Price
      if (currentDir == "LONG") {
         pipDistance = ((Bid - topEntry) / MarketInfo(OrderSymbol(), MODE_POINT) / 10);
         LabelTextChange(0, topPipsValue, DoubleToString(pipDistance, 2));
         pipDistance = ((Bid - bottomEntry) / MarketInfo(OrderSymbol(), MODE_POINT) / 10);
         LabelTextChange(0, bottomPipsValue, DoubleToString(pipDistance, 2));
      }
      
      // Use Ask Price
      if (currentDir == "SHORT") {
         pipDistance = ((topEntry - Ask) / MarketInfo(OrderSymbol(), MODE_POINT) / 10);
         LabelTextChange(0, topPipsValue, DoubleToString(pipDistance, 2));
         pipDistance = ((bottomEntry - Ask) / MarketInfo(OrderSymbol(), MODE_POINT) / 10);
         LabelTextChange(0, bottomPipsValue, DoubleToString(pipDistance, 2));
      }
      
      // Add Together Profit, Calculate % Return
      totalProfit = 0;
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in onTick"); }
         
         if (OrderSymbol() == Symbol()) {
            totalProfit = totalProfit + OrderProfit() + OrderSwap() - OrderCommission();
         }
      }
      
      // Calculate Return
      totalReturn = (totalProfit / AccountBalance()) * 100;
      
      
      LabelTextChange(0, returnValue, "$" + DoubleToString(totalProfit, 2) + " (" + DoubleToString(totalReturn, 2) + "%)");
      if (totalReturn >= 0) { 
         color toSet = clrWhite;
         if (totalReturn > 0) { toSet = clrLimeGreen; }
         
         ObjectSetInteger(0, returnValue, OBJPROP_COLOR, toSet); 
      } else {
         ObjectSetInteger(0, returnValue, OBJPROP_COLOR, clrRed);
      }
      
      // Potential Return based on TP
      potReturn = 0;
      potReturnPerc = 0;
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i, SELECT_BY_POS)) { Print("Error with OrderSelect in onTick"); }
         
         if (OrderSymbol() == Symbol() && OrderTakeProfit() != 0) {
            // Get TP Of Order in pips
            potCurPips = 0;
            
            if (OrderType() == OP_BUY)
               potCurPips = ((OrderTakeProfit() - OrderOpenPrice()) / MarketInfo(OrderSymbol(), MODE_POINT) / 10);
            else if (OrderType() == OP_SELL)
               potCurPips = ((OrderOpenPrice() - OrderTakeProfit()) / MarketInfo(OrderSymbol(), MODE_POINT) / 10);
            
            // Get Size of Order
            potCurLots = OrderLots();
            
            // Get Profit/Pip
            potDollarPerPip = ((((MarketInfo(Symbol(),MODE_TICKVALUE)* Point) / MarketInfo(Symbol(), MODE_TICKSIZE)) * potCurLots) * 10);
            
            // Calculate potential Return on this trade
            potCurReturn = potDollarPerPip * potCurPips;
            
            // Add to potential return
            potReturn = potReturn + potCurReturn;
         }
      }
      
      potReturnPerc = (potReturn / AccountBalance()) * 100;
      
      if (potReturn == 0) {
         LabelTextChange(0, potentialReturn, "");
      } else {
         LabelTextChange(0, potentialReturn, "TP: $" + DoubleToString(potReturn, 2) + " (" + DoubleToString(potReturnPerc, 2) + "%)");
      }
      
      // Potential Return based on SL
      potReturn = 0;
      potReturnPerc = 0;
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i, SELECT_BY_POS)) { Print("Error with OrderSelect in onTick"); }
         
         if (OrderSymbol() == Symbol() && OrderStopLoss() != 0) {
            // Get SL Of Order in pips
            potCurPips = 0;
            
            if (OrderType() == OP_BUY)
               potCurPips = ((OrderOpenPrice() - OrderStopLoss()) / MarketInfo(OrderSymbol(), MODE_POINT) / 10);
            else if (OrderType() == OP_SELL)
               potCurPips = ((OrderStopLoss() - OrderOpenPrice()) / MarketInfo(OrderSymbol(), MODE_POINT) / 10);
            
            // Get Size of Order
            potCurLots = OrderLots();
            
            // Get Profit/Pip
            potDollarPerPip = ((((MarketInfo(Symbol(),MODE_TICKVALUE)* Point) / MarketInfo(Symbol(), MODE_TICKSIZE)) * potCurLots) * 10);
            
            // Calculate potential Return on this trade
            potCurReturn = potDollarPerPip * -potCurPips;
            
            // Add to potential return
            potReturn = potReturn + potCurReturn;
         }
      }
      
      potReturnPerc = (potReturn / AccountBalance()) * 100;
      
      if (potReturn == 0) {
         LabelTextChange(0, potentialSLReturn, "");
      } else {
         LabelTextChange(0, potentialSLReturn, "SL: $" + DoubleToString(potReturn, 2) + " (" + DoubleToString(potReturnPerc, 2) + "%)");
      }
      
   } else {
      LabelTextChange(0, topPipsValue, "0");
      LabelTextChange(0, bottomPipsValue, "0");
      LabelTextChange(0, lotsValue, "0");
      LabelTextChange(0, returnValue, "");
      LabelTextChange(0, potentialReturn, "");
      LabelTextChange(0, potentialSLReturn, "");
      trailingStop = 0;
      takeProfit = 0;
      stopLoss = 0;
      lotSize = 0;
      
      // No orders open on the pair, so show TP levels for BUY/SELL
      if (tpLinesDrawn) {
         // Update the levels
         // Get the levels needed
         EditTextGet(curTP, 0, tpName);        
         takeProfit = StringToDouble(curTP);
         TPBuyLevel = Ask + (takeProfit / 10000);
         TPSellLevel = Bid - (takeProfit / 10000);
         
         HLineMove(0, TPLineBUY, TPBuyLevel);
         HLineMove(0, TPLineSELL, TPSellLevel);
         
         takeProfit = 0;         
      } else {
         // No lines, create them
         // Get the levels needed
         EditTextGet(curTP, 0, tpName);        
         takeProfit = StringToDouble(curTP);
         TPBuyLevel = Ask + (takeProfit / 10000);
         TPSellLevel = Bid - (takeProfit / 10000);
         
         HLineCreate(0, TPLineBUY, 0, TPBuyLevel, HLineColor, HLineStyle, HLineWidth, HLineBack, HLineSelection, HLineHidden, HLineZOrder);
         HLineCreate(0, TPLineSELL, 0, TPSellLevel, HLineColor, HLineStyle, HLineWidth, HLineBack, HLineSelection, HLineHidden, HLineZOrder);
         
         takeProfit = 0;
         tpLinesDrawn = true;
      }
   }
}



//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,           // Event identifier  
                  const long& lparam,     // Event parameter of long type
                  const double& dparam,   // Event parameter of double type
                  const string& sparam) { // Event parameter of string type

   //--- the mouse has been clicked on the graphic object
   if (id == CHARTEVENT_OBJECT_CLICK) {
      
      // Check buy button
      if (sparam == buyButton && safetyMode == false) {
         // Turn on Safety
         safetyMode = true;                                                // Turn on Safety
         ObjectSetInteger(0, safetyButton, OBJPROP_BGCOLOR, clrRed);       // Change to Red
         ObjectSetString(0, safetyButton, OBJPROP_TEXT, "S");              // Set to S
         
         // Get the values of the inputs
         EditTextGet(curTP, 0, tpName);
         EditTextGet(curSL, 0, slName);
         EditTextGet(curSize, 0, sizeName);
         
         takeProfit = StringToDouble(curTP);
         stopLoss = StringToDouble(curSL);
         lotSize = StringToDouble(curSize);
         
         if (takeProfit > 0) { takeProfit = Ask + (takeProfit / 10000); }
         if (stopLoss > 0) {   stopLoss   = Ask - (stopLoss / 10000); }
         
         // If we have orders open right now, get the current TP they are using and use that value instead
         if (ordersOnPair > 0) {
            for(int i = OrdersTotal() - 1; i >= 0; i--) {
               if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in onTick"); }
               if (OrderSymbol() == Symbol() && OrderType() == OP_BUY) {
                  takeProfit = OrderTakeProfit();
               }
            }
         }
         
         // Add entry here
         if (lotSize > 0) {   
            ticket = OrderSend(Symbol(), OP_BUY, lotSize, Ask, theSlippage, stopLoss, takeProfit, oComment, MGN, 0, clrNONE);
            currentDir = "LONG";
            
            // Delete the short button if needed
            if (sellButtonThere == true) { ButtonDelete(0, sellButton); sellButtonThere = false; }
            
            // Create the mode switch
            currentMode = "TOP";
            if (highResolution) {
               ButtonCreate(0, modeButton, 0, 100, 50, 90, 50, butCorner, currentMode, butFont, 8, butColor, clrAqua, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
            } else {
               ButtonCreate(0, modeButton, 0, 55, 20, 50, 20, butCorner, currentMode, butFont, 8, butColor, clrAqua, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
            }
         }
         
         // Adjust the new entry value
         EditTextChange(0, sizeName, DoubleToString(lotSize + incrementSize, 2));
         
         takeProfit = 0;
         stopLoss = 0;
         lotSize = 0;
         
         // Reset the button
         ObjectSetInteger(0, buyButton, OBJPROP_STATE, false);
      }
      
      
      // Check sell button
      if (sparam == sellButton && safetyMode == false) {
         // Turn on Safety
         safetyMode = true;                                                // Turn on Safety
         ObjectSetInteger(0, safetyButton, OBJPROP_BGCOLOR, clrRed);       // Change to Red
         ObjectSetString(0, safetyButton, OBJPROP_TEXT, "S");              // Set to S
      
         // Get the values of the inputs
         EditTextGet(curTP, 0, tpName);
         EditTextGet(curSL, 0, slName);
         EditTextGet(curSize, 0, sizeName);
         
         takeProfit = StringToDouble(curTP);
         stopLoss = StringToDouble(curSL);
         lotSize = StringToDouble(curSize);
         
         if (takeProfit > 0) { takeProfit = Bid - (takeProfit / 10000); }
         if (stopLoss > 0) {   stopLoss   = Bid + (stopLoss / 10000); }
         
         // If we have orders open right now, get the current TP they are using and use that value instead
         if (ordersOnPair > 0) {
            for(int i = OrdersTotal() - 1; i >= 0; i--) {
               if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in onTick"); }
               if (OrderSymbol() == Symbol() && OrderType() == OP_SELL) {
                  takeProfit = OrderTakeProfit();
               }
            }
         }
         
         // Add entry here
         if (lotSize > 0) {
            ticket = OrderSend(Symbol(), OP_SELL, lotSize, Bid, theSlippage, stopLoss, takeProfit, oComment, MGN, 0, clrNONE);
            currentDir = "SHORT";
            
            // Delete the long button if needed
            if (buyButtonThere == true) { ButtonDelete(0, buyButton); buyButtonThere = false; }
            
            // Create the mode switch
            currentMode = "BOTTOM";
            if (highResolution) {
               ButtonCreate(0, modeButton, 0, 210, 50, 90, 50, butCorner, currentMode, butFont, 8, butColor, clrAqua, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
            } else {
               ButtonCreate(0, modeButton, 0, 115, 20, 50, 20, butCorner, currentMode, butFont, 8, butColor, clrAqua, butBorderColor, butState, butBack, butSelection, butHidden, butZOrder);
            }
         }
         
         // Adjust the new entry value
         EditTextChange(0, sizeName, DoubleToString(lotSize + incrementSize, 2));
         
         takeProfit = 0;
         stopLoss = 0;
         lotSize = 0;
      }      
      
      // Check close in profit button
      if (sparam == profitButton && safetyMode == false) {
         // Turn on Safety
         safetyMode = true;                                                // Turn on Safety
         ObjectSetInteger(0, safetyButton, OBJPROP_BGCOLOR, clrRed);       // Change to Red
         ObjectSetString(0, safetyButton, OBJPROP_TEXT, "S");              // Set to S
         
         // Add entry here
         CloseAllProfitOrders();
      }
      
      // Check close all button
      if (sparam == closeButton && safetyMode == false) {
         // Turn on Safety
         safetyMode = true;                                                // Turn on Safety
         ObjectSetInteger(0, safetyButton, OBJPROP_BGCOLOR, clrRed);       // Change to Red
         ObjectSetString(0, safetyButton, OBJPROP_TEXT, "S");              // Set to S
         
         // Add entry here
         CloseAllOrders();
         
         // Stop Loop
         trailingStop = 0;
         takeProfit = 0;
         stopLoss = 0;
         lotSize = 0;
         EditTextChange(0, sizeName, DoubleToString(startSize, 2));
      }
      
      // Check Add TP button
      if (sparam == addTPButton) {
         // Get the values of the inputs needed
         EditTextGet(curTP, 0, tpName);
         takeProfit = StringToDouble(curTP);
         
         if (takeProfit > 0) {
            // Depending on the direction
            if (currentDir == "LONG") {
               // If we want the top or the bottom
               if (currentMode == "TOP") {
                  // Get the level of the highest trade
                  tpLevel = GetTopTrade();
               }
               
               if (currentMode == "BOTTOM") {
                  tpLevel = GetBottomTrade();
               }
               
               // Set the TP's accordingly
               theTarget = tpLevel + (takeProfit / 10000);
               
               // Edit the orders
               for(int i = OrdersTotal() - 1; i >= 0; i--) {
                  if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in addTP/SL Buttons"); }
               
                  if (OrderSymbol() == Symbol()) {
                     if (OrderType() == OP_BUY) {
                        
                        // Modify Order
                        if(!OrderModify (OrderTicket(), OrderOpenPrice(), OrderStopLoss(), theTarget, 0, clrNONE)) {
                           Print("Failed to modify order in addTPButton");
                        }
                     }
                  }
               }
            }
            
            if (currentDir == "SHORT") {
               // If we want the top or the bottom
               if (currentMode == "TOP") {
                  // Get the level of the highest trade
                  tpLevel = GetTopTrade();
               }
               
               if (currentMode == "BOTTOM") {
                  tpLevel = GetBottomTrade();
               }
               
               // Set the TP's accordingly
               theTarget = tpLevel - (takeProfit / 10000);
               
               // Edit the orders
               for(int i = OrdersTotal() - 1; i >= 0; i--) {
                  if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in addTP/SL Buttons"); }
               
                  if (OrderSymbol() == Symbol()) {
                     if (OrderType() == OP_SELL) {
                        
                        // Modify Order
                        if(!OrderModify (OrderTicket(), OrderOpenPrice(), OrderStopLoss(), theTarget, 0, clrNONE)) {
                           Print("Failed to modify order in addTPButton");
                        }
                     }
                  }
               }
            }
         }
         
         takeProfit = 0;
         stopLoss = 0;
         lotSize = 0;
      }
      
      // Check Remove TP button
      if (sparam == remTPButton) {         
         // Edit the orders
         for(int i = OrdersTotal() - 1; i >= 0; i--) {
            if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in addTP/SL Buttons"); }
         
            if (OrderSymbol() == Symbol()) {
               if (OrderType() == OP_BUY) {
               
                  // Modify Order
                  if(!OrderModify (OrderTicket(), OrderOpenPrice(), OrderStopLoss(), 0, 0, clrNONE)) {
                        Print("Failed to modify order in remTPButton");
                     }
               } else if (OrderType() == OP_SELL) {
                  
                  // Modify the Order
                  if(!OrderModify (OrderTicket(), OrderOpenPrice(), OrderStopLoss(), 0, 0, clrNONE)) {
                        Print("Failed to modify order in remTPButton");
                     }
               }
            }
         }
         
         takeProfit = 0;
         stopLoss = 0;
         lotSize = 0;
      }
      
      // Check Add SL button
      if (sparam == addSLButton) {
         // Get the values of the inputs needed
         EditTextGet(curSL, 0, slName);
         stopLoss = StringToDouble(curSL);
         
         if (stopLoss > 0) {
            // Depending on the direction
            if (currentDir == "LONG") {
               // If we want the top or the bottom
               if (currentMode == "TOP") {
                  // Get the level of the highest trade
                  slLevel = GetTopTrade();
               }
               
               if (currentMode == "BOTTOM") {
                  slLevel = GetBottomTrade();
               }
               
               // Set the SL's accordingly
               theTarget = slLevel - (stopLoss / 10000);
               
               // Edit the orders
               for(int i = OrdersTotal() - 1; i >= 0; i--) {
                  if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in addTP/SL Buttons"); }
               
                  if (OrderSymbol() == Symbol()) {
                     if (OrderType() == OP_BUY) {
                        
                        // Modify Order
                        if(!OrderModify (OrderTicket(), OrderOpenPrice(), theTarget, OrderTakeProfit(), 0, clrNONE)) {
                           Print("Failed to modify order in addSLButton");
                        }
                     }
                  }
               }
            }

            if (currentDir == "SHORT") {
               // If we want the top or the bottom
               if (currentMode == "TOP") {
                  // Get the level of the highest trade
                  slLevel = GetTopTrade();
               }
               
               if (currentMode == "BOTTOM") {
                  slLevel = GetBottomTrade();
               }
               
               // Set the SL's accordingly
               theTarget = slLevel - (stopLoss / 10000);
               
               // Edit the orders
               for(int i = OrdersTotal() - 1; i >= 0; i--) {
                  if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in addTP/SL Buttons"); }
               
                  if (OrderSymbol() == Symbol()) {
                     if (OrderType() == OP_SELL) {
                        
                        // Modify Order
                        if(!OrderModify (OrderTicket(), OrderOpenPrice(), theTarget, OrderTakeProfit(), 0, clrNONE)) {
                           Print("Failed to modify order in addSLButton");
                        }
                     }
                  }
               }
            }
         }
         
         takeProfit = 0;
         stopLoss = 0;
         lotSize = 0;
      }
      
      // Check Remove SL button
      if (sparam == remSLButton) {         
         // Edit the orders
         for(int i = OrdersTotal() - 1; i >= 0; i--) {
            if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in addTP/SL Buttons"); }
         
            if (OrderSymbol() == Symbol()) {
               if (OrderType() == OP_BUY) {
               
                  // Modify Order
                  if(!OrderModify (OrderTicket(), OrderOpenPrice(), 0, OrderTakeProfit(), 0, clrNONE)) {
                        Print("Failed to modify order in remTPButton");
                     }
               } else if (OrderType() == OP_SELL) {
                  
                  // Modify the Order
                  if(!OrderModify (OrderTicket(), OrderOpenPrice(), 0, OrderTakeProfit(), 0, clrNONE)) {
                        Print("Failed to modify order in remTPButton");
                     }
               }
            }
         }
         
         takeProfit = 0;
         stopLoss = 0;
         lotSize = 0;
      }
      
      // Check add trailing stop button
      if (sparam == addTrailButton) {
      
         // Get the Trailing SL Value
         EditTextGet(curTrail, 0, trailName);
         trailingStop = StringToDouble(curTrail);
      }      
      
      // Check remove trailing stop button
      if (sparam == remTrailButton) {
         // Stop Loop
         trailingStop = 0;
         
         // Remove Stop Losses
         // Edit the orders
         for(int i = OrdersTotal() - 1; i >= 0; i--) {
            if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in remove trailing stop Buttons"); }
         
            if (OrderSymbol() == Symbol()) {
               if (OrderType() == OP_BUY) {
               
                  // Modify Order
                  if(!OrderModify (OrderTicket(), OrderOpenPrice(), 0, OrderTakeProfit(), 0, clrNONE)) {
                        Print("Failed to modify order in rem trailing stop button");
                     }
               } else if (OrderType() == OP_SELL) {
                  
                  // Modify the Order
                  if(!OrderModify (OrderTicket(), OrderOpenPrice(), 0, OrderTakeProfit(), 0, clrNONE)) {
                        Print("Failed to modify order in rem trailing stop button");
                     }
               }
            }
         }
      }
      
      if (sparam == safetyButton) {
         // If safety is on, turn off, change colors
         if (safetyMode) {
            safetyMode = false;                                               // Turn off Safety
            ObjectSetInteger(0, safetyButton, OBJPROP_BGCOLOR, clrLimeGreen); // Change to Green
            ObjectSetString(0, safetyButton, OBJPROP_TEXT, "F");              // Set to F
         } else {
            safetyMode = true;                                                // Turn on Safety
            ObjectSetInteger(0, safetyButton, OBJPROP_BGCOLOR, clrRed);       // Change to Red
            ObjectSetString(0, safetyButton, OBJPROP_TEXT, "S");              // Set to S
         }
      }    
      
      if (sparam == modeButton) {
         // Change the text of the button
         if (currentMode == "TOP") {
            currentMode = "BOTTOM";
            ObjectSetString(0, modeButton, OBJPROP_TEXT, currentMode);
         } else if (currentMode == "BOTTOM") {
            currentMode = "TOP";
            ObjectSetString(0, modeButton, OBJPROP_TEXT, currentMode);
         }
      }    
      
      ObjectSetInteger(0, buyButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, sellButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, closeButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, profitButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, addSLButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, remSLButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, addTPButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, remTPButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, addTrailButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, remTrailButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, safetyButton, OBJPROP_STATE, false);
      ObjectSetInteger(0, modeButton, OBJPROP_STATE, false);
     
      Sleep(40); // Pause for 40 ms
   }
}

//+------------------------------------------------------------------+
//| Trail all the order                                              |
//+------------------------------------------------------------------+
void TrailAllOrders(double trail) {
   double stopcrnt;
   double stopcal;
   int trade;
   int trades = OrdersTotal();
   double profitcalc;
   
   if (trail > 0) {
      for(trade = 0;trade < trades; trade++) {
         if (!OrderSelect(trade, SELECT_BY_POS, MODE_TRADES)) { Print("Error Selecting Ticket in TrailAllOrders"); }
         if(OrderSymbol() == Symbol()) {
            
            //LONG
            if(currentDir == "LONG" && OrderType() == OP_BUY) {
               stopcrnt = OrderStopLoss();
               stopcal = Bid - (trail * Point);
               profitcalc = OrderTakeProfit() + (OrderTakeProfit() * Point);
               if (stopcrnt == 0) {
                  if (!OrderModify(OrderTicket(),OrderOpenPrice(),stopcal,profitcalc,0,Blue)) { Print("Error Modifying Ticket in TrailAllOrders"); }
               } else if (stopcal > stopcrnt) {
                  if (!OrderModify(OrderTicket(),OrderOpenPrice(),stopcal,profitcalc,0,Blue)) { Print("Error Modifying Ticket in TrailAllOrders"); }
               }
            }
            
            //SHORT
            if (currentDir == "SHORT" && OrderType() == OP_SELL) {
               stopcrnt = OrderStopLoss();
               stopcal = Ask + (trail * Point);
               profitcalc = OrderTakeProfit() - (OrderTakeProfit() * Point);
               if (stopcrnt == 0) {
                  if (!OrderModify(OrderTicket(),OrderOpenPrice(),stopcal,profitcalc,0,Red)) { Print("Error Modifying Ticket in TrailAllOrders"); }
               } else if (stopcal < stopcrnt) {
                  if (!OrderModify(OrderTicket(),OrderOpenPrice(),stopcal,profitcalc,0,Red)) { Print("Error Modifying Ticket in TrailAllOrders"); }
               }
            }
         }
      }
   }
}

//+------------------------------------------------------------------+
//| Get Top Trade                                                    |
//+------------------------------------------------------------------+
double GetTopTrade() {  
   double level = 0;
   
   if (currentDir == "LONG") {     
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in GetTopTrade"); }
            
         if (OrderSymbol() == Symbol()) {
            if (OrderType() == OP_BUY) {
               if (OrderOpenPrice() > level) { level = OrderOpenPrice(); }
            }
         }
      }
   }
   
   if (currentDir == "SHORT") {     
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in GetTopTrade"); }
            
         if (OrderSymbol() == Symbol()) {
            if (OrderType() == OP_SELL) {
               if (OrderOpenPrice() > level) { level = OrderOpenPrice(); }
            }
         }
      }
   }
   
   return(level);
}

//+------------------------------------------------------------------+
//| Get Bottom Trade                                                 |
//+------------------------------------------------------------------+
double GetBottomTrade() {  
   double level = 1000000;
   
   if (currentDir == "LONG") {     
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in GetTopTrade"); }
            
         if (OrderSymbol() == Symbol()) {
            if (OrderType() == OP_BUY) {
               if (OrderOpenPrice() < level) { level = OrderOpenPrice(); }
            }
         }
      }
   }
   
   if (currentDir == "SHORT") {     
      for(int i = OrdersTotal() - 1; i >= 0; i--) {
         if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in GetTopTrade"); }
            
         if (OrderSymbol() == Symbol()) {
            if (OrderType() == OP_SELL) {
               if (OrderOpenPrice() < level) { level = OrderOpenPrice(); }
            }
         }
      }
   }
   
   return(level);
}

//+------------------------------------------------------------------+
//| Calculate Break Even Level                                       |
//+------------------------------------------------------------------+
double CalculateBreakEven() {
   int PipAdjust = 0;
   int NrOfDigits = Digits;
   double point = 0;
   
   if(NrOfDigits == 5 || NrOfDigits == 3) {
      PipAdjust = 10; 
   } else if(NrOfDigits == 4 || NrOfDigits == 2) {
      PipAdjust = 1;
   }
   
   point = Point * PipAdjust;
   
   // Long Variables
   int Total_Buy_Trades = 0;
   double Total_Buy_Size = 0;
   double Total_Buy_Price = 0;
   double Buy_Profit = 0;
   
   // Short Variables
   int Total_Sell_Trades = 0;
   double Total_Sell_Size = 0;
   double Total_Sell_Price = 0;
   double Sell_Profit = 0;

   // Net Variables
   int Net_Trades = 0;
   double Net_Lots = 0;
   double Net_Result = 0;

   // Calculated Variables
   double Average_Price = 0;
   double distance = 0;
   double Pip_Value = MarketInfo(Symbol(), MODE_TICKVALUE) * PipAdjust;
   double Pip_Size = MarketInfo(Symbol(), MODE_TICKSIZE) * PipAdjust;

   // Get total number of orders
   int total = OrdersTotal();

   // Loop through the orders
   for(int i=0; i < total; i++) {
      int ord = OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      
      if(OrderType() == OP_BUY && OrderSymbol() == Symbol()) {
         Total_Buy_Trades++;
         Total_Buy_Price += OrderOpenPrice() * OrderLots();
         Total_Buy_Size += OrderLots();
         Buy_Profit += OrderProfit() + OrderSwap() + OrderCommission();
      }
      
      if(OrderType() == OP_SELL && OrderSymbol() == Symbol()) {
         Total_Sell_Trades++;
         Total_Sell_Size += OrderLots();
         Total_Sell_Price += OrderOpenPrice() * OrderLots();
         Sell_Profit += OrderProfit() + OrderSwap() + OrderCommission();
      }
   }
   
   if(Total_Buy_Price > 0) {
      Total_Buy_Price /= Total_Buy_Size;
   }
   
   if(Total_Sell_Price > 0) {
      Total_Sell_Price /= Total_Sell_Size;
   }
   
   Net_Trades = Total_Buy_Trades + Total_Sell_Trades;
   Net_Lots = Total_Buy_Size - Total_Sell_Size;
   Net_Result = Buy_Profit + Sell_Profit;

   
   if(Net_Trades > 0 && Net_Lots != 0) {
      distance = (Net_Result / (MathAbs(Net_Lots * MarketInfo(Symbol(), MODE_TICKVALUE))) * MarketInfo(Symbol(), MODE_TICKSIZE));
      
      if(Net_Lots > 0) {
         Average_Price = Bid - distance;
      }
      
      if(Net_Lots < 0) {
         Average_Price = Ask + distance;
      }
     }
     
   if(Net_Trades > 0 && Net_Lots == 0) {
      distance = (Net_Result / ((MarketInfo(Symbol(), MODE_TICKVALUE))) * MarketInfo(Symbol(), MODE_TICKSIZE));
      Average_Price = Bid - distance;
   }

   return(Average_Price);
}

//+------------------------------------------------------------------+
//| Close all the orders in profit                                   |
//+------------------------------------------------------------------+
void CloseAllProfitOrders() {
   for(int i = OrdersTotal() - 1; i >= 0; i--) {
      if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in CloseAllOrders"); }
      
      if (OrderSymbol() == Symbol() && OrderProfit() > 0) {
         if (!OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),5)) { Print("Error with OrderClose in CloseAllOrders"); }
      }
   }
}

//+------------------------------------------------------------------+
//| Close all the orders                                             |
//+------------------------------------------------------------------+
void CloseAllOrders() {
   for(int i = OrdersTotal() - 1; i >= 0; i--) {
      if (!OrderSelect(i,SELECT_BY_POS)) { Print("Error with OrderSelect in CloseAllOrders"); }
      
      if (OrderSymbol() == Symbol()) {
         if(OrderType()==OP_BUY || OrderType()==OP_SELL) {
            if (!OrderClose(OrderTicket(),OrderLots(),OrderClosePrice(),5)) { Print("Error with OrderClose in CloseAllOrders"); }
         } else {
            if (!OrderDelete(OrderTicket())) { Print("Error with OrderDelete in CloseAllOrders"); }
         }
      }
   }
}

//+------------------------------------------------------------------+
//| Create the button                                                |
//+------------------------------------------------------------------+
bool ButtonCreate(const long              chart_ID=0,               // chart's ID
                  const string            name="Button",            // button name
                  const int               sub_window=0,             // subwindow index
                  const int               x=0,                      // X coordinate
                  const int               y=0,                      // Y coordinate
                  const int               width=50,                 // button width
                  const int               height=18,                // button height
                  const ENUM_BASE_CORNER  corner=CORNER_RIGHT_UPPER,// chart corner for anchoring
                  const string            text="Button",            // text
                  const string            font="Arial",             // font
                  const int               font_size=10,             // font size
                  const color             clr=clrBlack,             // text color
                  const color             back_clr=C'236,233,216',  // background color
                  const color             border_clr=clrNONE,       // border color
                  const bool              state=false,              // pressed/released
                  const bool              back=false,               // in the background
                  const bool              selection=false,          // highlight to move
                  const bool              hidden=true,              // hidden in the object list
                  const long              z_order=0) {              // priority for mouse click
                  
   //--- reset the error value
   ResetLastError();
   //--- create the button
   if (!ObjectCreate(chart_ID,name,OBJ_BUTTON,sub_window,0,0)) {
      Print(__FUNCTION__, ": failed to create the button! Error code = ", GetLastError());
      return(false);
   }
   //--- set button coordinates
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
   //--- set button size
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width);
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height);
   //--- set the chart's corner, relative to which point coordinates are defined
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);
   //--- set the text
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
   //--- set text font
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
   //--- set font size
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
   //--- set text color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
   //--- set background color
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr);
   //--- set border color
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_COLOR,border_clr);
   //--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
   //--- set button state
   ObjectSetInteger(chart_ID,name,OBJPROP_STATE,state);
   //--- enable (true) or disable (false) the mode of moving the button by mouse
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
   //--- hide (true) or display (false) graphical object name in the object list
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
   //--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
   
   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------+
//| Delete the button                                                |
//+------------------------------------------------------------------+
bool ButtonDelete(const long   chart_ID=0,      // chart's ID
                  const string name="Button") { // button name

   //--- reset the error value
   ResetLastError();
   //--- delete the button
   if (!ObjectDelete(chart_ID, name)) {
      Print(__FUNCTION__, ": failed to delete the button! Error code = ", GetLastError());
      return(false);
   }
   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------+
//| Create a text label                                              |
//+------------------------------------------------------------------+
bool LabelCreate(const long              chart_ID=0,               // chart's ID
                 const string            name="Label",             // label name
                 const int               sub_window=0,             // subwindow index
                 const int               x=0,                      // X coordinate
                 const int               y=0,                      // Y coordinate
                 const ENUM_BASE_CORNER  corner=CORNER_LEFT_UPPER, // chart corner for anchoring
                 const string            text="Label",             // text
                 const string            font="Arial",             // font
                 const int               font_size=10,             // font size
                 const color             clr=clrRed,               // color
                 const double            angle=0.0,                // text slope
                 const ENUM_ANCHOR_POINT anchor=ANCHOR_LEFT_UPPER, // anchor type
                 const bool              back=false,               // in the background
                 const bool              selection=false,          // highlight to move
                 const bool              hidden=true,              // hidden in the object list
                 const long              z_order=0) {              // priority for mouse click

   //--- reset the error value
   ResetLastError();
   //--- create a text label
   if (!ObjectCreate(chart_ID, name, OBJ_LABEL, sub_window, 0, 0)) {
      Print(__FUNCTION__, ": failed to create text label! Error code = ", GetLastError());
      return(false);
   }
   //--- set label coordinates
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
   //--- set the chart's corner, relative to which point coordinates are defined
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);
   //--- set the text
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
   //--- set text font
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
   //--- set font size
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
   //--- set the slope angle of the text
   ObjectSetDouble(chart_ID,name,OBJPROP_ANGLE,angle);
   //--- set anchor type
   ObjectSetInteger(chart_ID,name,OBJPROP_ANCHOR,anchor);
   //--- set color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
   //--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
   //--- enable (true) or disable (false) the mode of moving the label by mouse
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
   //--- hide (true) or display (false) graphical object name in the object list
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
   //--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------+
//| Change the object text                                           |
//+------------------------------------------------------------------+
bool LabelTextChange(const long   chart_ID=0,   // chart's ID
                     const string name="Label", // object name
                     const string text="Text") {// text

   //--- reset the error value
   ResetLastError();
   //--- change object text
   if (!ObjectSetString(chart_ID,name, OBJPROP_TEXT, text)) {
      Print(__FUNCTION__, ": failed to change the text! Error code = ", GetLastError());
      return(false);
   }
   
   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------+
//| Delete a text label                                              |
//+------------------------------------------------------------------+
bool LabelDelete(const long   chart_ID=0,      // chart's ID
                 const string name="Label") {  // label name

   //--- reset the error value
   ResetLastError();
   //--- delete the label
   if (!ObjectDelete(chart_ID,name)) {
      Print(__FUNCTION__, ": failed to delete a text label! Error code = ", GetLastError());
      return(false);
   }
   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------+
//| Create Edit object                                               |
//+------------------------------------------------------------------+
bool EditCreate(const long             chart_ID=0,               // chart's ID
                const string           name="Edit",              // object name
                const int              sub_window=0,             // subwindow index
                const int              x=0,                      // X coordinate
                const int              y=0,                      // Y coordinate
                const int              width=50,                 // width
                const int              height=18,                // height
                const string           text="Text",              // text
                const string           font="Arial",             // font
                const int              font_size=10,             // font size
                const ENUM_ALIGN_MODE  align=ALIGN_CENTER,       // alignment type
                const bool             read_only=false,          // ability to edit
                const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER, // chart corner for anchoring
                const color            clr=clrBlack,             // text color
                const color            back_clr=clrWhite,        // background color
                const color            border_clr=clrNONE,       // border color
                const bool             back=false,               // in the background
                const bool             selection=false,          // highlight to move
                const bool             hidden=true,              // hidden in the object list
                const long             z_order=0) {              // priority for mouse click

   //--- reset the error value
   ResetLastError();
   //--- create edit field
   if(!ObjectCreate(chart_ID, name, OBJ_EDIT, sub_window, 0, 0)) {
      Print(__FUNCTION__, ": failed to create \"Edit\" object! Error code = ", GetLastError());
      return(false);
   }
   
   //--- set object coordinates
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
   //--- set object size
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width);
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height);
   //--- set the text
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
   //--- set text font
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
   //--- set font size
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
   //--- set the type of text alignment in the object
   ObjectSetInteger(chart_ID,name,OBJPROP_ALIGN,align);
   //--- enable (true) or cancel (false) read-only mode
   ObjectSetInteger(chart_ID,name,OBJPROP_READONLY,read_only);
   //--- set the chart's corner, relative to which object coordinates are defined
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);
   //--- set text color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
   //--- set background color
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr);
   //--- set border color
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_COLOR,border_clr);
   //--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
   //--- enable (true) or disable (false) the mode of moving the label by mouse
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
   //--- hide (true) or display (false) graphical object name in the object list
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
   //--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
   
   //--- successful execution
   return(true);
}
  
//+------------------------------------------------------------------+
//| Change Edit object's text                                        |
//+------------------------------------------------------------------+
bool EditTextChange(const long   chart_ID=0,     // chart's ID
                    const string name="Edit",    // object name
                    const string text="Text") {  // text
   //--- reset the error value
   ResetLastError();
   //--- change object text
   if(!ObjectSetString(chart_ID, name, OBJPROP_TEXT, text)) {
      Print(__FUNCTION__, ": failed to change the text! Error code = ", GetLastError());
      return(false);
   }
   
   //--- successful execution
   return(true);
  }
  
//+------------------------------------------------------------------+
//| Return Edit object text                                          |
//+------------------------------------------------------------------+
bool EditTextGet(string      &text,           // text
                 const long   chart_ID=0,     // chart's ID
                 const string name="Edit") {  // object name

   //--- reset the error value
   ResetLastError();
   //--- get object text
   if(!ObjectGetString(chart_ID, name, OBJPROP_TEXT, 0, text)) {
      Print(__FUNCTION__, ": failed to get the text! Error code = ", GetLastError());
      return(false);
   }
   
   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------+
//| Delete Edit object                                               |
//+------------------------------------------------------------------+
bool EditDelete(const long   chart_ID=0,     // chart's ID
                const string name="Edit") {  // object name

   //--- reset the error value
   ResetLastError();
   //--- delete the label
   if(!ObjectDelete(chart_ID, name)) {
      Print(__FUNCTION__, ": failed to delete \"Edit\" object! Error code = ", GetLastError());
      return(false);
   }
   
   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------+
//| Create the horizontal line                                       |
//+------------------------------------------------------------------+
bool HLineCreate(const long            chart_ID=0,        // chart's ID
                 const string          name="HLine",      // line name
                 const int             sub_window=0,      // subwindow index
                 double                price=0,           // line price
                 const color           clr=clrRed,        // line color
                 const ENUM_LINE_STYLE style=STYLE_SOLID, // line style
                 const int             width=1,           // line width
                 const bool            back=false,        // in the background
                 const bool            selection=true,    // highlight to move
                 const bool            hidden=true,       // hidden in the object list
                 const long            z_order=0) {       // priority for mouse click

   //--- if the price is not set, set it at the current Bid price level
   if(!price) { price=SymbolInfoDouble(Symbol(),SYMBOL_BID); }
   
   //--- reset the error value
   ResetLastError();
   
   //--- create a horizontal line
   if(!ObjectCreate(chart_ID,name,OBJ_HLINE,sub_window,0,price)) {
      Print(__FUNCTION__, ": failed to create a horizontal line! Error code = ",GetLastError());
      return(false);
   }

   //--- set line color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
   //--- set line display style
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style);
   //--- set line width
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,width);
   //--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
   //--- enable (true) or disable (false) the mode of moving the line by mouse
   //--- when creating a graphical object using ObjectCreate function, the object cannot be
   //--- highlighted and moved by default. Inside this method, selection parameter
   //--- is true by default making it possible to highlight and move the object
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
   //--- hide (true) or display (false) graphical object name in the object list
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
   //--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------+
//| Move horizontal line                                             |
//+------------------------------------------------------------------+
bool HLineMove(const long   chart_ID=0,   // chart's ID
               const string name="HLine", // line name
               double       price=0) {    // line price
               
   //--- if the line price is not set, move it to the current Bid price level
   if(!price) { price=SymbolInfoDouble(Symbol(),SYMBOL_BID); }
   
   //--- reset the error value
   ResetLastError();
   
   //--- move a horizontal line
   if(!ObjectMove(chart_ID,name,0,0,price)) {
      Print(__FUNCTION__, ": failed to move the horizontal line! Error code = ",GetLastError());
      return(false);
   }

   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------+
//| Delete a horizontal line                                         |
//+------------------------------------------------------------------+
bool HLineDelete(const long   chart_ID=0,     // chart's ID
                 const string name="HLine") { // line name
   
   //--- reset the error value
   ResetLastError();
   
   //--- delete a horizontal line
   if(!ObjectDelete(chart_ID,name)) {
      Print(__FUNCTION__, ": failed to delete a horizontal line! Error code = ",GetLastError());
      return(false);
   }
   
   //--- successful execution
   return(true);
}

//+------------------------------------------------------------------------------------+
//| The function defines if the mode of shift of the price chart from the right border |
//| is enabled.                                                                        |
//+------------------------------------------------------------------------------------+
bool ChartShiftGet(bool &result,const long chart_ID=0) {
   //--- prepare the variable to get the property value
   long value;
   //--- reset the error value
   ResetLastError();
   //--- receive the property value
   if(!ChartGetInteger(chart_ID,CHART_SHIFT,0,value)) {
      //--- display the error message in Experts journal
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
   }
   //--- store the value of the chart property in memory
   result=value;
   //--- successful execution
   return(true);
}

//+--------------------------------------------------------------------------+
//| The function enables/disables the mode of displaying a price chart with  |
//| a shift from the right border.                                           |
//+--------------------------------------------------------------------------+
bool ChartShiftSet(const bool value,const long chart_ID=0) {
   //--- reset the error value
   ResetLastError();
   //--- set property value
   if(!ChartSetInteger(chart_ID,CHART_SHIFT,0,value)) {
      //--- display the error message in Experts journal
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
   }
   //--- successful execution
   return(true);
}

//+--------------------------------------------------------------------------+
//| Get the history of today/yesterday/week/month/year/all time              |
//+--------------------------------------------------------------------------+
void returnHistory() {
   double today     = 0;
   double yesterday = 0;
   double week      = 0;
   double month     = 0;
   double year      = 0;
   double alltime   = 0;
   
   for(int i = OrdersHistoryTotal() - 1; i >= 0; i--) {
      if (!OrderSelect(i, SELECT_BY_POS, MODE_HISTORY)) { Print("Error with OrderSelect in returnHistory"); }
      
      if (OrderSymbol() == Symbol()) {
         if(OrderType()== OP_BUY || OrderType() == OP_SELL) {
            if (TimeYear(OrderCloseTime()) == TimeYear(TimeLocal()) && 
                TimeDayOfYear(OrderCloseTime()) == TimeDayOfYear(TimeLocal())) { 
               today = today + OrderProfit() - OrderCommission() + OrderSwap(); 
            }
            
            if (TimeYear(OrderCloseTime()) == TimeYear(TimeLocal()) && 
                TimeDayOfYear(OrderCloseTime()) == TimeDayOfYear(TimeLocal()) - 1) { 
               yesterday = yesterday + OrderProfit() - OrderCommission() + OrderSwap(); 
            }
            
            if (TimeYear(OrderCloseTime()) == TimeYear(TimeLocal()) && 
                TimeDayOfYear(OrderCloseTime()) >= TimeDayOfYear(TimeLocal()) - 7) { 
               week = week + OrderProfit() - OrderCommission() + OrderSwap();
            }
            
            if (TimeYear(OrderCloseTime()) == TimeYear(TimeLocal()) && 
                TimeMonth(OrderCloseTime()) == TimeMonth(TimeLocal())) { 
               month = month + OrderProfit() - OrderCommission() + OrderSwap();
            }
            
            if (TimeYear(OrderCloseTime()) == TimeYear(TimeLocal())) { 
               year = year + OrderProfit() - OrderCommission() + OrderSwap();
            }
            
            alltime = alltime + OrderProfit() - OrderCommission() + OrderSwap(); 
         }
      }
   }
   
   plToday = today;
   plYesterday = yesterday;
   plWeek = week;
   plMonth = month;
   plYTD = year;
   plAllTime = alltime;
}